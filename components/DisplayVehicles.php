<?php namespace Monologophobia\Vehicles\Components;

use Redirect;
use Cms\Classes\Page;
use Monologophobia\Vehicles\Models\Vehicle;
use Monologophobia\Vehicles\Models\Feature;

class DisplayVehicles extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name' => 'Display Vehicles',
            'description' => 'Display a list of Vehicles or details about a specific one'
        ];
    }

    public function defineProperties() {
        return [
            'vehicle_id' => [
                'title' => 'Vehicle ID',
                'type'  => 'text'
            ],
            'quotepage' => [
                'title'   => 'Quote Page',
                'type'    => 'dropdown',
            ]
        ];
    }

    public function getQuotePageOptions() {
        return Page::sortBy('baseFileName')->lists('title', 'url');
    }

    public function onRun() {
        $vehicle_id = intval($this->property('vehicle_id'));
        if ($vehicle = Vehicle::find($vehicle_id)) {
            $this->page['header']  = $vehicle->general_images[0]->getPath();
            $this->page['vehicle'] = $vehicle;
        }
        else {
            $this->page['vehicles'] = Vehicle::get();
        }
        $this->page['features'] = Feature::get();
        $this->page['quote_page'] = $this->property('quotepage');
    }

}
