<?php namespace Monologophobia\Vehicles\Components;

use Redirect;
use Monologophobia\Vehicles\Models\Quote;

class Quotes extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name' => 'Quotes',
            'description' => 'Displays quotes and payments'
        ];
    }

    public function defineProperties() {
        return [
            'quote_id' => [
                'title' => 'Quote ID',
                'type'  => 'text'
            ]
        ];
    }

    public function onRun() {
        $quote_id = intval($this->property('quote_id'));
        if ($quote = Quote::find($quote_id)) {
            if ($quote->deposit_paid) {
                return Redirect::to('/')->with('message', 'Quote already paid');
            }
            $this->page['quote'] = $quote;
        }
        else {
            $this->setStatusCode(404);
            return $this->controller->run('404');
        }
    }

}
