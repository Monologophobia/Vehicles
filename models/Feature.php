<?php namespace Monologophobia\Vehicles\Models;

use \October\Rain\Database\Model;

class Feature extends Model {

    // The table to use
    public $table = 'mono_vehicles_features';
    public $timestamps = true;

    protected $nullable = ['options'];
    protected $jsonable = ['options'];

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name' => 'required|string'
    ];

    public $hasManyThrough = [
        'vehicles' => [
            'Monologophobia\Vehicles\Models\Vehicle',
            'key'        => 'feature_id',
            'through'    => 'Monologophobia\Vehicles\Models\VehicleFeature',
            'throughKey' => 'vehicle_id',
            'pivot'      => 'option_id'
        ]
    ];

    public function beforeSave() {
        if ($this->options) {
            foreach ($this->options as $option) {
                $option['quote_price'] = number_format(floatval($this['quote_price']), 2);
            }
        }
    }

    public function afterFetch() {
        if ($this->options) {
            foreach ($this->options as $option) {
                $option['quote_price'] = number_format(floatval($this['quote_price']), 2);
            }
        }
    }

}