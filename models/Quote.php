<?php namespace Monologophobia\Vehicles\Models;

use \October\Rain\Database\Model;

class Quote extends Model {

    // The table to use
    public $table = 'mono_vehicles_quotes';
    public $timestamps = true;

    protected $nullable = ['notes'];

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name'    => 'required|string',
        'email'   => 'required|string',
        'vehicle' => 'required|string'
    ];

    public function beforeCreate() {
        $price = 0;
        foreach ($this->details as $detail) {
            if ($feature = Feature::find($detail)) {
                $price += $feature->quote_price;
            }
        }
        $this->price = $price;
        $this->quote_price = ($price ? number_format($price / 2, 2) : 0);
    }

    public function beforeSave() {
        $this->price = number_format(floatval($this->price), 2);
        $this->quote_price = number_format(floatval($this->quote_price), 2);
    }

    public function afterFetch() {
        $features = [];
        foreach ($this->details as $detail) {
            if ($feature = Feature::find($detail)) {
                $features[] = $feature->name;
            }
        }
        $this->features = $features;
        $this->price = number_format(floatval($this->price), 2);
        $this->quote_price = number_format(floatval($this->quote_price), 2);
    }

}