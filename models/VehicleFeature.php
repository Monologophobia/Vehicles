<?php namespace Monologophobia\Vehicles\Models;

use \October\Rain\Database\Model;

use Monologophobia\Vehicles\Models\Feature;

class VehicleFeature extends Model {

    // The table to use
    public $table = 'mono_vehicles_vehicles_features';
    public $timestamps = true;

    public $belongsTo = [
        'vehicle' => ['Monologophobia\Vehicles\Models\Vehicle'],
        'feature' => ['Monologophobia\Vehicles\Models\Feature'],
    ];

    public function getFeatureIdOptions() {
        return Feature::orderBy('name', 'asc')->lists('name', 'id');
    }

    public $attachOne = [
        'image' => ['System\Models\File'],
    ];

    public function getOptionIdOptions() {
        if (!$this->feature_id) {
             $feature = Feature::orderBy('name', 'asc')->first();
             $this->feature_id = $feature->id;
        }
        $feature = Feature::find($this->feature_id);
        if (!$feature->options) return [0 => "No Options"];
        $options = [];
        foreach ($feature->options as $option) {
            $options[] = $option['name'];
        }
        return $options;
    }

}
