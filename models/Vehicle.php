<?php namespace Monologophobia\Vehicles\Models;

use \October\Rain\Database\Model;

use Monologophobia\Vehicles\Models\Feature;

class Vehicle extends Model {

    // The table to use
    public $table = 'mono_vehicles_vehicles';
    public $timestamps = true;

    protected $nullable = ['short_description', 'description', 'price', 'specifications'];
    protected $jsonable = ['specifications', 'features'];

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name' => 'required|string'
    ];

    public $belongsToMany = [
        'features' => [
            'Monologophobia\Vehicles\Models\Feature',
            'key'      => 'vehicle_id',
            'table'    => 'mono_vehicles_vehicles_features',
            'otherKey' => 'feature_id',
            'pivot'    => 'option_id'
        ]
    ];

    public $attachMany = [
        'general_images'  => ['System\Models\File'],
        'external_images' => ['System\Models\File'],
        'internal_images' => ['System\Models\File'],
        'engine_images'   => ['System\Models\File'],
    ];

    public $hasMany = [
        'vehicle_features' => ['Monologophobia\Vehicles\Models\VehicleFeature'],
    ];

    public function beforeSave() {
        $this->price = number_format(floatval($this->price), 2);
    }

    public function afterFetch() {
        $this->price = number_format(floatval($this->price), 2);
    }

}