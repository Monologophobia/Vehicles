<?php namespace Monologophobia\Vehicles\Controllers;

use Redirect;
use BackendMenu;
use Backend\Classes\Controller;
use Monologophobia\Vehicles\Models\Feature;

class Features extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Vehicles', 'vehicles', 'features');
    }

    public function index_onDelete() {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            try {
                foreach ($checkedIds as $id) {
                    $feature = Feature::findOrFail($id);
                    $feature->delete();
                }
                Flash::success('Deleted');
            }
            catch (\Exception $e) {
                Flash::error($e->getMessage());
            }
            return $this->listRefresh();
        }
    }

}
