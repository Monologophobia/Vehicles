<?php namespace Monologophobia\Vehicles\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class onePointZeroPointThree extends Migration {

    public function up() {

        Schema::create('mono_vehicles_vehicles_features', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('vehicle_id')->unsigned()->index();
            $table->foreign('vehicle_id')->references('id')->on('mono_vehicles_vehicles')->onDelete('cascade');
            $table->integer('feature_id')->unsigned()->index();
            $table->foreign('feature_id')->references('id')->on('mono_vehicles_features')->onDelete('cascade');
            $table->integer('option_id')->default(0);
            $table->timestamps();
        });

    }

    public function down() {
        Schema::dropIfExists('mono_vehicles_vehicles_features');
    }

}