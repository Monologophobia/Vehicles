<?php namespace Monologophobia\Vehicles\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class onePointZeroPointZero extends Migration {

    public function up() {

        Schema::create('mono_vehicles_vehicles', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->decimal('price', 2)->nullable();
            $table->text('specifications')->nullable();
            $table->timestamps();
        });

        Schema::create('mono_vehicles_features', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->decimal('quote_price', 2);
            $table->timestamps();
        });

        Schema::create('mono_vehicles_vehicles_features', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('vehicle_id')->unsigned()->index();
            $table->foreign('vehicle_id')->references('id')->on('mono_vehicles_vehicles')->onDelete('cascade');
            $table->integer('feature_id')->unsigned()->index();
            $table->foreign('feature_id')->references('id')->on('mono_vehicles_features')->onDelete('cascade');
            $table->text('description');
            $table->timestamps();
        });

        Schema::create('mono_vehicles_quotes', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('vehicle');
            $table->boolean('own_vehicle')->default(false);
            $table->text('name');
            $table->string('email');
            $table->text('details');
            $table->decimal('price', 2)->default(0);
            $table->decimal('deposit', 2)->default(0);
            $table->boolean('deposit_paid')->default(false);
            $table->text('notes')->nullable();
            $table->timestamps();
        });

    }

    public function down() {
        Schema::dropIfExists('mono_vehicles_vehicles_specifications');
        Schema::dropIfExists('mono_vehicles_specifications');
        Schema::dropIfExists('mono_vehicles_vehicles');
        Schema::dropIfExists('mono_vehicles_quotes');
    }

}
