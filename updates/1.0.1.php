<?php namespace Monologophobia\Vehicles\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class onePointZeroPointOne extends Migration {

    public function up() {

        Schema::dropIfExists('mono_vehicles_vehicles_specifications');

        Schema::table('mono_vehicles_vehicles', function($table) {
            $table->string('logo')->nullable();
            $table->decimal('price', 8, 2)->nullable()->change();
            $table->text('features')->nullable();
        });

        Schema::table('mono_vehicles_features', function($table) {
            $table->decimal('quote_price', 8, 2)->nullable()->change();
        });

        Schema::table('mono_vehicles_quotes', function($table) {
            $table->decimal('price', 8, 2)->default(0)->change();
            $table->decimal('deposit', 8, 2)->default(0)->change();
        });
    }

    public function down() {

        Schema::table('mono_vehicles_vehicles', function($table) {
            $table->dropColumn('logo');
            $table->dropColumn('features');
            $table->decimal('price', 2)->nullable()->change();
        });

        Schema::table('mono_vehicles_features', function($table) {
            $table->decimal('quote_price', 8, 2)->nullable()->change();
        });

        Schema::table('mono_vehicles_quotes', function($table) {
            $table->decimal('price', 2)->default(0)->change();
            $table->decimal('deposit', 2)->default(0)->change();
        });

        Schema::create('mono_vehicles_vehicles_features', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('vehicle_id')->unsigned()->index();
            $table->foreign('vehicle_id')->references('id')->on('mono_vehicles_vehicles')->onDelete('cascade');
            $table->integer('feature_id')->unsigned()->index();
            $table->foreign('feature_id')->references('id')->on('mono_vehicles_features')->onDelete('cascade');
            $table->text('description');
            $table->timestamps();
        });

    }

}
