<?php namespace Monologophobia\Vehicles\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class onePointZeroPointTwo extends Migration {

    public function up() {

        Schema::table('mono_vehicles_features', function($table) {
            $table->dropColumn('quote_price');
            $table->text('options')->nullable();
        });

    }

    public function down() {

        Schema::table('mono_vehicles_features', function($table) {
            $table->dropColumn('options');
            $table->decimal('quote_price', 8, 2)->nullable();
        });

    }

}
