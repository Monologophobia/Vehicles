<?php namespace Monologophobia\Vehicles;

use Event;
use Backend\Facades\Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase {

    // Returns details about the plugin
    public function pluginDetails() {
        return [
            'name'        => 'Vehicles',
            'description' => 'Find Universities',
            'author'      => 'Monologophobia',
            'icon'        => 'icon-car'
        ];
    }

    // Create the backend navigation
    public function registerNavigation() {
        return [
            'vehicles' => [
                'label' => 'Vehicles',
                'url'   => Backend::url('monologophobia/vehicles/vehicles'),
                'icon'  => 'icon-car',
                'order' => 601,

                'sideMenu' => [
                    'vehicles' => [
                        'label' => 'Vehicles',
                        'url'   => Backend::url('monologophobia/vehicles/vehicles'),
                        'icon'  => 'icon-car'
                    ],
                    'features' => [
                        'label' => 'Features',
                        'url'   => Backend::url('monologophobia/vehicles/features'),
                        'icon'  => 'icon-list'
                    ],
                    'quotes' => [
                        'label' => 'Quotes',
                        'url'   => Backend::url('monologophobia/vehicles/quotes'),
                        'icon'  => 'icon-money'
                    ]
                ]
            ]
        ];
    }

    public function registerComponents() {
        return [
           '\Monologophobia\Vehicles\Components\DisplayVehicles' => 'displayvehicles',
           '\Monologophobia\Vehicles\Components\Quotes' => 'quotes',
        ];
    }

    public function registerMailTemplates() {
        return [
            'monologophobia.vehicles::vehicles.enquiry' => 'Enquiry email',
        ];
    }

}